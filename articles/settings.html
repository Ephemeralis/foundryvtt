<p>Foundry Virtual Tabletop features a number of configurable settings which are accessed by clicking the <strong>Configure Settings</strong> button in the <strong>Settings Sidebar</strong> tab.</p>

<p class="note info">Note that these settings differ from the @Article[hosting] settings which are controlled by the server host.</p>

<h3 id="scopes">Setting Scopes</h3>
<p>There are two different setting scopes which are applied:</p>
<ul>
    <li><strong>Client Settings</strong> which are saved in the local storage of the User's browser. As a consequence, client settings will apply to all Worlds that are accessed from the same device using the same browser. The Maximum Framerate is an example of a client setting. Any user can modify their own client-level settings.</li>
    <li><strong>World Settings</strong> which are stored in the server-side database for the World. As a consequence, world settings only apply to a single World but are common to all Users within that World. Whether Chat Bubbles are enabled is an example of a world-level setting. By default, only a Gamemaster or User with the "Assistant Gamemaster" role can modify world-level settings.</li>
</ul>
<hr/>

<h3 id="core">Core Software Settings</h3>
<p>The following configurable settings are supported by the core Foundry Virtual Tabletop software on the "Core Settings" tab. </p>

<figure>
    @Image[35]
    <figcaption>The Core Settings which can be configured by a gamemaster user in the Settings Sidebar.</figcaption>
</figure>

<dl>
    <dt>Configure Permissions</dt>
    <dd>Open a separate configuration menu to customize permission controls for different User roles. See the section specific to Permission Configuration below for more details.</dd>

    <dt>Configure Audio/Video</dt>
    <dd>Foundry Virtual Tabletop supports optional configuration of Audio/Video chat integration using WebRTC. This button opens a separate dialog window for configuration of this feature. See the <a href="/audio-video" title="Audio/Video Chat Integration" target="_blank"></a> guide for more details.</dd>

    <dt>Language Preference</dt>
    <dd><strong>[Client]</strong> Select a supported language which you would like to use for Foundry Virtual Tabletop. English is supported by default and other languages may be installed as Modules. If a translation module is installed any languages which it defines will appear as an option in this dropdown list.</dd>

    <dt>Enable Chat Bubbles</dt>
    <dd><strong>[World]</strong> If this setting is enabled, in-character chat messages which originate from an Actor who has a Token on the active Scene will display a chat bubble above the position of that Token for all players.</dd>

    <dt>Pan to Token Speaker</dt>
    <dd><strong>[World]</strong> If this setting is enabled, and Enable Chat Bubbles is also enabled, when a Token chat bubble is displayed the camera position of the Scene will automatically pan to the location of the speaking Token.</dd>

    <dt>Maximum Framerate</dt>
    <dd><strong>[Client]</strong> Set a maximum framerate at which the Foundry Virtual Tabletop canvas will be rendered. By default the canvas renders at 60fps, but setting a lower maximum value may be helpful to limit the resource consumption of the Foundry application on less powerful machines.</dd>

    <dt>Enable Soft Shadows</dt>
    <dd><strong>[Client]</strong> Enable or disable the blur effect which is applied to dynamic lighting and fog of war which makes the edges of light sources and shadows "soft". Disabling this setting may offer improved performance for machines with lower-end hardware.</dd>

    <dt>Animate Roll Tables</dt>
    <dd><strong>[World]</strong> If this setting is enabled, Roll Tables will display a roulette-style animation when results are drawn and the Table Configuration sheet is currently open.</dd>

    <dt>Cone Template Type</dt>
    <dd><strong>[World]</strong> Choose which template measurement style should be used for cones. Flat cones will draw a straight line between the two outer rays while rounded cones will include a radial arc based on the angle of the cone.</dd>
</dl>

<p>Changes to these game settings are only applied when the <strong>Save Changes</strong> button is clicked and some changes may require the browser to automatically refresh in order to take effect. All settings can be reset back to their default values by clicking the <strong>Reset Defaults</strong> button.</p>
<hr>

<h3 id="permissions">Permission Configuration</h3>
<p>Clicking on the <strong>Permission Configuration</strong> button of the settings menu allows the customization of specific permissions which can be assigned or removed from different User roles.</p>

<p class="note info">Visit the @Article[users] documentation to learn more about User roles and what they represent.</p>

<p>Each Permission can be assigned to a set of Roles which are able to perform that permission. Some permissions cannot be revoked for a Gamemaster role while others can be disabled even for Gamemaster users. The image below shows the Permission Configuration form and the list which follows describes each permission capability.</p>

<figure>
    @Image[36]
    <figcaption>The Permission Configuration submenu allows for fine-tuned customization of the permissions allowed to each User role.</figcaption>
</figure>

<dl>
    <dt>Allow Broadcasting Audio</dt>
    <dd>Allow players with this role to broadcast audio when using A/V chat integration.</dd>

    <dt>Allow Broadcasting Video</dt>
    <dd>Allow players with this role to broadcast video when using A/V chat integration.</dd>

    <dt>Browse File Explorer</dt>
    <dd>Allow players with this role to browse through available files in the File Picker.</dd>

    <dt>Configure Token Settings</dt>
    <dd>Allow players with this role to configure the settings for Tokens that they own.</dd>

    <dt>Create Journal Entries</dt>
    <dd>Allow players with this role to create new Journal Entries in the Journal sidebar.</dd>

    <dt>Create Measured Template</dt>
    <dd>Allow players with this role to create templates for area-of-effect measurement.</dd>

    <dt>Create New Actors</dt>
    <dd>Allow players with this role to create new Actors in the World.</dd>

    <dt>Create New Items</dt>
    <dd>Allow players with this role to create new Items in the World.</dd>

    <dt>Create New Tokens</dt>
    <dd>Allow players with this role to place Tokens for Actors they own on the game canvas.</dd>

    <dt>Display Mouse Cursor</dt>
    <dd>Display the position of the player's mouse cursor which illustrates where the user is looking on the canvas.</dd>

    <dt>Modify Configuration Settings</dt>
    <dd>Allow players with this role to modify game settings and enable or disable modules.</dd>

    <dt>Open and Close Doors</dt>
    <dd>Allow players with this role interact with doors defined on the Walls Layer.</dd>

    <dt>Upload New Files</dt>
    <dd>Allow players with this role to upload content to the server using the File Picker.</dd>

    <dt>Use Drawing Tools</dt>
    <dd>Allow players with this role to create Drawings using the drawing tools.</dd>

    <dt>Use Script Macros</dt>
    <dd>Allow players with this role to use JavaScript macros which access the API.</dd>

    <dt>Whisper Private Messages</dt>
    <dd>Allow players with this role whisper private messages that the Game Master cannot see.</dd>
</dl>

<p>Changes to Permission Configuration are only applied when the <strong>Save Configuration</strong> button is clicked and some changes may require the browser to automatically refresh in order to take effect. All settings can be reset back to their default values by clicking the <strong>Reset Defaults</strong> button.</p>
<hr>

<h3 id="api">API References</h3>
<p>Module and system developers have the capability to register their own settings and settings menus using the Foundry API. This involves working with the "ClientSettings" API (although this also manages world settings) to register settings and settings menus. Some examples are provided below, but see the API documentation for more detail.</p>

<ul>
	<li>The @API[ClientSettings,The ClientSettings Manager] Manager</li>
</ul>

<h4>Defining a Game Setting</h4>
<pre><code class="language-js">// Define a new setting which can be stored and retrieved
game.settings.register("myModule", "mySetting", {
  name: "Register a Module Setting with Choices",
  hint: "A description of the registered setting and its behavior.",
  scope: "client",
  config: true,
  type: String,
  choices: {
    "a": "Option A",
    "b": "Option B"
  },
  default: "a",
  onChange: value => console.log(value)
});
</code></pre>

<h4>Defining a Settings Submenu</h4>
<pre><code class="language-js">// Define a settings submenu which handles advanced configuration needs
game.settings.registerMenu("myModule", "mySettingsMenu", {
  name: "My Settings Submenu",
  label: "The label which appears on the Settings submenu button",
  hint: "A description of what will occur in the submenu dialog.",
  icon: "fas fa-bars",
  type: MySubmenuApplicationClass,
  restricted: false
});
</code></pre>

<h4>Referencing and Assigning a Setting Value</h4>
<pre><code class="language-js">// Retrieve the current setting value
game.settings.get("myModule", "mySetting");
// Assign a new setting value
game.settings.set("myModule", "mySetting", "b");
</code></pre>
<hr/>
